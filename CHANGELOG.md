# Changelog

## V1.0.0 - 19/05/2019
### Added
+ ItemUtil
+ EntityUtil
+ Added broadcasts in messages

## V0.0.1 - 18/05/2019
### Added
+ Tabcompletion
+ Config class
+ Message class