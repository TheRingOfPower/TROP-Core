package io.gitlab.theringofpower.tropcore;

import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.plugin.java.JavaPlugin;

import java.io.File;
import java.io.IOException;
import java.util.logging.Level;

public class CustomConfig {
    private JavaPlugin plugin;
    private String fileName;
    private File configFile;
    private YamlConfiguration config;

    public CustomConfig(JavaPlugin plugin, String fileName) {
        this.plugin = plugin;
        this.fileName = fileName;
        configFile = new File(plugin.getDataFolder(), fileName);
    }

    public void reloadConfig() {
        config = YamlConfiguration.loadConfiguration(configFile);
    }

    public YamlConfiguration getConfig() {
        if (config == null) reloadConfig();

        return config;
    }

    public void saveConfig() {
        if (config == null || configFile == null) return;

        try {
            getConfig().save(configFile);
        } catch (IOException e) {
            plugin.getLogger().log(Level.SEVERE, "Could not save config to " + configFile, e);
        }
    }

    public void saveDefaultConfig() {
        if (configFile.exists()) return;

        plugin.saveResource(fileName, false);
    }
}
