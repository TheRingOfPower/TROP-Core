package io.gitlab.theringofpower.tropcore;

import java.util.Collection;
import org.bukkit.ChatColor;
import org.bukkit.Server;
import org.bukkit.World;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class Messages {
	private String prefix;
	
	public Messages(String prefix) {
		this.prefix = prefix;
	}
	
	public void setPrefix(String prefix) {
		this.prefix = prefix;
	}
	
	public void sendMessage(CommandSender target, String message) {
		target.sendMessage(ChatColor.translateAlternateColorCodes('&', prefix + " " + message));
	}
	
	public void sendNeedPlayerMessage(CommandSender target) {
		sendMessage(target, "&cYou need to be a player to execute this command.");
	}

	public void sendInvalidArgumentMessage(CommandSender target) {
		sendMessage(target, "&cInvalid amount of arguments.");
	}
	
	public void broadcast(World world, String message) {
		broadcast(world.getPlayers(), message);
	}
	
	public void broadcast(Server server, String message) {
		broadcast(server.getOnlinePlayers(), message);
	}
	
	public void broadcast(Collection<? extends Player> players, String message) {
		for(Player player : players) sendMessage(player, message);
	}
}
