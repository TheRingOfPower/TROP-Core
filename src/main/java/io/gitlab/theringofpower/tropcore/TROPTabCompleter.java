package io.gitlab.theringofpower.tropcore;

import java.util.List;

import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.command.TabCompleter;
import org.bukkit.entity.Player;

public abstract class TROPTabCompleter implements TabCompleter {
	private String cmdName;
	
	public TROPTabCompleter(String cmdName) {
		this.cmdName = cmdName;
	}

	@Override
	public List<String> onTabComplete(CommandSender sender, Command cmd, String label, String[] args) {
		if(!(sender instanceof Player) || !cmd.getName().equalsIgnoreCase(cmdName)) return null;
		
		List<String> argsList = getArgList((Player) sender, args);

		if(argsList != null && !argsList.isEmpty()) {
			int argIndex = args.length - 1;
			
			if(args[argIndex] != null) {
				argsList.removeIf(s -> !s.toLowerCase().contains(args[argIndex].toLowerCase()));
			}
			if(!argsList.isEmpty()) return argsList;
		}
		
		
		return null;
	}
	
	public abstract List<String> getArgList(Player sender, String[] args);
}
