package io.gitlab.theringofpower.tropcore.utils;

import org.bukkit.entity.EntityType;

public class EntityUtil {
	
	public static EntityType getEntityFromID(String id) {
		String name = id.toUpperCase();
        name = name.replace(":", "_");
        name = name.replace(".", "");
        
		return EntityType.valueOf(name);
	}
}
